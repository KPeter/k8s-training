This lab tutorials are intended to lead you through the process how you can build different infrastructure (DevOps/GitOps platform, etc.) on Kubernetes.

# License

<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc.png" alt="license" style="width:200px;"/> 

>This license enables reusers to distribute, remix, adapt, and build upon the material in any medium or format for noncommercial purposes only, and only so long as attribution is given to the creator. 
>
>CC BY-NC includes the following elements:
>
> BY: credit must be given to the creator.
> 
> NC: Only noncommercial uses of the work are permitted.


# Request for Changes

Please test everything:
* If you find some bug please raise an issue or create a fix/merge request
* If you'd like to see other labs create an issue for requesting implementation

# Pre-requisites

To successfully achieve the goal you need a LAB environment. You can buy/rent one kubernetes solution from the cloud providers (AWS, GCP, Azure, DigitalOcean, etc) or build your own using virtualization.
In this tutorials we use Linux KVM and minikube to have a kubernetes cluster and build different lab environments, see [Preparation](docs/preparation/ReadMe.md)

# Required basic knowledge

* Linux command line

# What we will learn

* How to use minikube
* How to use Kubernetes tools like kubectl
* How to provision resources with helm or kubectl
* How to provision resources with Terraform
* How to configure Keycloak SSO service
* How to use ArgoCD Application kind to quickly install resources in a GitOps way

# Basic Infrastructure

The basic services are installed manually using Helm charts or with Terraform

Basic services are:
* MetalLB
* Traefik
* ExternalDNS with one of the providers on the host (CoreDNS or PowerDNS)
* Cert-manager
* Ldap (for external Keycloak users and groups)
* Keycloak
* ArgoCD
* Crossplane

The installation of these components is on [Preparation](docs/preparation/ReadMe.md) page

# GitOps service - ArgoCD

All other workloads are installed through ArgoCD. All the charts are basic charts that uses the original chart as dependencies, eg.: my grafana chart is just a chart that points to the official grafana chart as dependencies and we specified the grafana chart values under the dependency "grafana:" chart (see the code in [Custom Applications](custom-apps/) )

## Lab environments

* [Building Kubernetes Monitoring](docs/01_monitoring_lab/ReadMe.md)
* [Prometheus and alerting](docs/02_prometheus_lab/ReadMe.md)
* [Kubernetes single sign-on with Keycloak](docs/03_k8s_oidc/ReadMe.md)
* [Manage Cloud resources with Crossplane](docs/05_crossplane_lab/ReadMe.md)


# ToDos

* Add a storage provider as basic service (rook, minio or something that could be run outside also)
* Showcase to  create cloud resources through Crossplane with terraform provider and with native provider