#!/usr/bin/python3

from http.server import BaseHTTPRequestHandler, HTTPServer
import time

hostName = "0.0.0.0"
serverPort = 5000

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        print ("MY SERVER: I got a GET request.")
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

    def do_POST(self):
        print ("MY SERVER: I got a POST request.")
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        print("POST request\nPath: %s\nBody:%s\n" % (str(self.path),
            post_data.decode('utf-8') ) )
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
 

if __name__ == "__main__":        
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")

