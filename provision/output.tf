output "cert-manager-CA-cert" {
  value     = module.cert-manager.CA-cert
  sensitive = true
}

output "cert-manager-CA-key" {
  value     = module.cert-manager.CA-key
  sensitive = true
}
