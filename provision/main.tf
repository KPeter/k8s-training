provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "lab"
}

provider "helm" {
  kubernetes {
    config_path    = "~/.kube/config"
    config_context = "lab"
  }
}

provider "keycloak" {
  client_id                = "admin-cli"
  username                 = "administrator"
  password                 = "Adm1nPassw0rd"
  url                      = "https://kc-console.nginx.k8s.local"
  tls_insecure_skip_verify = true
}

module "metallb" {

  source = "./modules/metallb"

  iprange = "192.168.39.2-192.168.39.149"
  metallb = {
    helm_chart_name    = "metallb"
    helm_repo_url      = "https://metallb.github.io/metallb"
    helm_chart_version = "0.13.12"
    helm_release_name  = "metallb"
    release_namespace  = "metallb"
  }

}

module "traefik" {

  source = "./modules/traefik"

  depends_on = [module.metallb]

  traefik = {
    helm_chart_name    = "traefik"
    helm_repo_url      = "https://helm.traefik.io/traefik"
    helm_chart_version = "25.0.0"
    helm_release_name  = "traefik"
    release_namespace  = "traefik"
  }

}

module "ingress-nginx" {

  source = "./modules/ingress-nginx"

  depends_on = [module.metallb]

  ingress-nginx = {
    helm_chart_name    = "ingress-nginx"
    helm_repo_url      = "https://kubernetes.github.io/ingress-nginx"
    helm_chart_version = "4.8.3"
    helm_release_name  = "ingress-nginx"
    release_namespace  = "ingress-nginx"
  }

}


module "cert-manager" {

  source = "./modules/cert-manager"

  cert-manager = {
    helm_chart_name    = "cert-manager"
    helm_repo_url      = "https://charts.jetstack.io"
    helm_chart_version = "1.13.1"
    helm_release_name  = "cert-manager"
    release_namespace  = "cert-manager"
  }

  # create a new cert or just use the one that created earlier
  # and sits in the certs directory
  createnewcerts = false

  certmgr-ca = {
    CN          = "ca.k8s.local"
    O           = "MyOrg"
    OU          = "IT"
    valid_hours = 43830
  }

  ca-issuer-secret-name = "ca-issuer"
}


module "externaldns" {
  source = "./modules/ExternalDNS-with-PowerDNS"

  externaldns = {
    helm_chart_name    = ""
    helm_repo_url      = "oci://registry-1.docker.io/bitnamicharts/external-dns"
    helm_chart_version = "6.28.4"
    helm_release_name  = "externaldns"
    release_namespace  = "externaldns"
  }

  powerdns = {
    apiurl = "http://hostmaster.local"
    apikey = "Zh4rsn+C4NdQTTsS"
  }
}


module "ldap" {

  source = "./modules/ldap"

  depends_on = [module.ingress-nginx]

  ldap = {
    release_namespace  = "ldap"
  }

  slapd = {
    root_dn = "dc=k8s,dc=local"
    admin_dn = "cn=admin,dc=k8s,dc=local"
    admin_user = "admin"
    admin_password = "adminpwd"
    config_admin_username = "configAdmin"
    config_admin_password = "adminpwd"
  }

}


module "keycloak" {

  source = "./modules/keycloak"

  depends_on = [module.ingress-nginx]

  keycloak = {
    helm_chart_name    = ""
    helm_repo_url      = "oci://registry-1.docker.io/bitnamicharts/keycloak"
    helm_chart_version = "17.3.5"
    helm_release_name  = "keycloak"
    release_namespace  = "keycloak"
  }

  secrets = {
    keycloakadminpass = "Adm1nPassw0rd"
    pgadminpass       = "pgadminpwd"
    pguserpass        = "pgkeycloakpwd"
  }

}

module "argocd" {
  source = "./modules/argocd"

  depends_on = [module.traefik, module.keycloak]

  argocd = {
    helm_release_name  = "argocd"
    helm_repo_url      = "https://argoproj.github.io/argo-helm"
    helm_chart_name    = "argo-cd"
    helm_chart_version = "5.51.3"
    release_namespace  = "argocd"
    ingress_url        = "argocd.traefik.k8s.local"
  }

  oidc = {
    CA_cert   = module.cert-manager.CA-cert
    idp_url   = "https://keycloak.nginx.k8s.local/realms/k8s-lab"
    client_id = "argocd"
  }
}
