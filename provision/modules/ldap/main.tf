resource "kubernetes_namespace" "ldap" {
  metadata {
    name = var.ldap.release_namespace
  }
}

resource "kubernetes_secret" "openldap-credentials" {
  metadata {
    namespace = var.ldap.release_namespace
    name = "openldap-credentials"
  }

  data = {
    adminpassword = var.slapd.admin_password
    configadminpassword = var.slapd.config_admin_password
  }
}

resource "kubernetes_config_map" "ldap_initialdata" {
  metadata {
    namespace = var.ldap.release_namespace
    name = "dsetree"
  }

  data = {
    "dsetree.ldif" = "${file("${path.module}/manifests/dsetree.ldif")}"
  }
}

resource "kubernetes_persistent_volume_claim" "ldap-pv" {
  metadata {
    namespace = var.ldap.release_namespace
    name = "ldap-pvc"
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "1Gi"
      }
    }
  }
}

resource "kubectl_manifest" "ldap-deplyment" {
  yaml_body = templatefile("${path.module}/manifests/openldap_deployment.yaml", 
    { 
      namespace = var.ldap.release_namespace,
      root_dn = var.slapd.root_dn,
      admin_dn = var.slapd.admin_dn,
      admin_user = var.slapd.admin_user,
      config_admin_username = var.slapd.config_admin_username
    })
}

resource "kubectl_manifest" "ldap-service" {
  yaml_body = templatefile("${path.module}/manifests/opedlap_service.yaml", { namespace = var.ldap.release_namespace })
}


