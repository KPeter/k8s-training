variable "ldap" {
  type = object({
    release_namespace = string
  })
}

variable "slapd" {
  type = object({
    root_dn = string
    admin_dn = string
    admin_user = string
    admin_password = string
    config_admin_username = string
    config_admin_password = string
  })
}
