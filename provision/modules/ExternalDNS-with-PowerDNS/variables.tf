variable "externaldns" {
  type = object({
    helm_release_name  = string
    helm_repo_url      = string
    helm_chart_name    = string
    helm_chart_version = string
    release_namespace  = string
  })
}

variable "powerdns" {
  type = object({
    apiurl = string
    apikey = string
  })
}