resource "kubernetes_namespace" "externaldns" {
  metadata {
    name = var.externaldns.release_namespace
  }
}

#Handle multiple manifest in one file
data "kubectl_file_documents" "rbac" {
  content = templatefile("${path.module}/manifests/rbac.yaml", { namespace = var.externaldns.release_namespace })
}

resource "kubectl_manifest" "extdns-rbac" {
  count     = length(data.kubectl_file_documents.rbac.documents)
  yaml_body = element(data.kubectl_file_documents.rbac.documents, count.index)
}

resource "helm_release" "externaldns" {
  name       = var.externaldns.helm_release_name
  repository = var.externaldns.helm_repo_url
  chart      = var.externaldns.helm_chart_name
  version    = var.externaldns.helm_chart_version
  namespace  = var.externaldns.release_namespace
  values = [
    "${templatefile("${path.module}/templates/values.yaml", { apiurl = var.powerdns.apiurl, apikey = var.powerdns.apikey })}"
  ]
  depends_on = [kubectl_manifest.extdns-rbac]
}
