variable "cert-manager" {
  type = object({
    helm_release_name  = string
    helm_repo_url      = string
    helm_chart_name    = string
    helm_chart_version = string
    release_namespace  = string
  })
}

variable "createnewcerts" {
  type = bool
}

variable "certmgr-ca" {
  type = object({
    CN          = string
    O           = string
    OU          = string
    valid_hours = number
  })
}

variable "ca-issuer-secret-name" {
  type = string
}