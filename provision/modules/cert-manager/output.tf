output "CA-key" {
  value = var.createnewcerts ? tls_private_key.certmgr-ca-key[0].private_key_pem : file("${path.module}/certs/CA.key")
}

output "CA-cert" {
  value = var.createnewcerts ? tls_self_signed_cert.certmgr-ca-cert[0].cert_pem : file("${path.module}/certs/CA.cert")
}