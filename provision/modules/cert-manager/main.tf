# Create the CA nad write out the data to files (for later use)
resource "tls_private_key" "certmgr-ca-key" {
  count     = var.createnewcerts ? 1 : 0
  algorithm = "RSA"

  # never destroy a key created earlier
  lifecycle {
    prevent_destroy = true
  }
}

resource "local_file" "certmgr-ca-key-file" {
  count    = var.createnewcerts ? 1 : 0
  content  = tls_private_key.certmgr-ca-key[count.index].private_key_pem
  filename = "${path.module}/certs/CA.key"

  # never destroy a key file created earlier
  lifecycle {
    prevent_destroy = true
  }
}

resource "tls_self_signed_cert" "certmgr-ca-cert" {
  count           = var.createnewcerts ? 1 : 0
  private_key_pem = tls_private_key.certmgr-ca-key[count.index].private_key_pem

  is_ca_certificate = true

  subject {
    common_name         = var.certmgr-ca.CN
    organization        = var.certmgr-ca.O
    organizational_unit = var.certmgr-ca.OU
  }

  validity_period_hours = var.certmgr-ca.valid_hours //  1825 days or 5 years

  allowed_uses = [
    "digital_signature",
    "cert_signing",
    "crl_signing",
  ]

  # never destroy a cert created earlier
  lifecycle {
    prevent_destroy = true
  }
}

resource "local_file" "certmgr-ca-cert-file" {
  count    = var.createnewcerts ? 1 : 0
  content  = tls_self_signed_cert.certmgr-ca-cert[count.index].cert_pem
  filename = "${path.module}/certs/CA.cert"

  # never destroy a cert file created earlier
  lifecycle {
    prevent_destroy = true
  }
}

# Install cert-manager

resource "kubernetes_namespace" "cert-manager" {
  metadata {
    name = var.cert-manager.release_namespace
  }
}

resource "helm_release" "cert-manager" {
  name       = var.cert-manager.helm_release_name
  repository = var.cert-manager.helm_repo_url
  chart      = var.cert-manager.helm_chart_name
  version    = var.cert-manager.helm_chart_version
  namespace  = var.cert-manager.release_namespace
  values = [
    "${file("${path.module}/templates/values.yaml")}"
  ]

}

resource "kubectl_manifest" "CA-data" {
  yaml_body = templatefile("${path.module}/manifests/CA-secret.yaml", {
    secretname = var.ca-issuer-secret-name,
    namespace  = var.cert-manager.release_namespace,
    cert       = var.createnewcerts ? tls_self_signed_cert.certmgr-ca-cert[0].cert_pem : file("${path.module}/certs/CA.cert"),
    key        = var.createnewcerts ? tls_private_key.certmgr-ca-key[0].private_key_pem : file("${path.module}/certs/CA.key")
  })
}

# Create cluster issuer
resource "kubectl_manifest" "cert-manager-cluster-issuer" {
  yaml_body  = templatefile("${path.module}/manifests/cluster-issuer.yaml", { secretname = var.ca-issuer-secret-name })
  depends_on = [helm_release.cert-manager]
}



