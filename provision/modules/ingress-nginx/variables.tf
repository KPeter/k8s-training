variable "ingress-nginx" {
  type = object({
    helm_release_name  = string
    helm_repo_url      = string
    helm_chart_name    = string
    helm_chart_version = string
    release_namespace  = string
  })
}