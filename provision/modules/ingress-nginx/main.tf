resource "kubernetes_namespace" "ingress-nginx" {
  metadata {
    name = var.ingress-nginx.release_namespace
  }
}

resource "helm_release" "ingress-nginx" {
  name       = var.ingress-nginx.helm_release_name
  repository = var.ingress-nginx.helm_repo_url
  chart      = var.ingress-nginx.helm_chart_name
  version    = var.ingress-nginx.helm_chart_version
  namespace  = var.ingress-nginx.release_namespace
  values = [
    "${file("${path.module}/templates/values.yaml")}"
  ]
}
