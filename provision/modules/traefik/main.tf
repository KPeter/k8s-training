resource "kubernetes_namespace" "traefik" {
  metadata {
    name = var.traefik.release_namespace
  }
}

resource "helm_release" "traefik" {
  name       = var.traefik.helm_release_name
  repository = var.traefik.helm_repo_url
  chart      = var.traefik.helm_chart_name
  version    = var.traefik.helm_chart_version
  namespace  = var.traefik.release_namespace
  values = [
    "${file("${path.module}/templates/values.yaml")}"
  ]
}
