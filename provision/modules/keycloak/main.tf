
resource "kubernetes_namespace" "keycloak" {
  metadata {
    name = var.keycloak.release_namespace
  }
}


resource "kubernetes_config_map" "realms" {
  metadata {
    name      = "keycloak-bootstrap"
    namespace = var.keycloak.release_namespace
  }

  data = {
    for f in fileset("${path.module}/realmconfigs", "*.json") :
    f => file(join("/", ["${path.module}/realmconfigs/", f]))
  }

}

resource "kubernetes_secret" "pgsecrets" {
  metadata {
    name      = "keycloak-postgres-secret"
    namespace = var.keycloak.release_namespace
  }

  data = {
    postgres-password = var.secrets.pgadminpass
    password          = var.secrets.pguserpass
  }

}

resource "kubernetes_secret" "keycloaksecret" {
  metadata {
    name      = "keycloak-admin-secret"
    namespace = var.keycloak.release_namespace
  }

  data = {
    password = var.secrets.keycloakadminpass
  }
}

resource "kubernetes_manifest" "admin-ingress" {
  manifest = yamldecode(file("${path.module}/templates/admin-ui-ingress.yaml"))
}

resource "kubernetes_manifest" "user-ingress" {
  manifest = yamldecode(file("${path.module}/templates/user-ui-ingress.yaml"))
}

resource "helm_release" "keycloak" {
  name       = var.keycloak.helm_release_name
  repository = var.keycloak.helm_repo_url
  chart      = var.keycloak.helm_chart_name
  version    = var.keycloak.helm_chart_version
  namespace  = var.keycloak.release_namespace
  values = [
    "${file("${path.module}/templates/values.yaml")}"
  ]
  depends_on = [
    kubernetes_namespace.keycloak,
    kubernetes_config_map.realms,
    kubernetes_secret.keycloaksecret,
    kubernetes_secret.pgsecrets
  ]
}


