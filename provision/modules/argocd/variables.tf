variable "argocd" {
  type = object({
    helm_release_name  = string
    helm_repo_url      = string
    helm_chart_name    = string
    helm_chart_version = string
    release_namespace  = string
    ingress_url        = string
  })
}

variable "oidc" {
  type = object({
    CA_cert       = string
    idp_url       = string
    client_id     = string
    //client_secret = string
  })
}
