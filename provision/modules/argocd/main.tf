resource "kubernetes_namespace" "argocd" {
  metadata {
    name = var.argocd.release_namespace
  }
}

resource "helm_release" "argocd" {
  name       = var.argocd.helm_release_name
  repository = var.argocd.helm_repo_url
  chart      = var.argocd.helm_chart_name
  version    = var.argocd.helm_chart_version
  namespace  = var.argocd.release_namespace
  values = [
    templatefile("${path.module}/templates/values.yaml", {
      ingress_url        = var.argocd.ingress_url,
      idp_url            = var.oidc.idp_url,
      oidc_client_id     = var.oidc.client_id,
      oidc_client_secret = data.keycloak_openid_client.argocd-client-secret.client_secret
      rootCA             = var.oidc.CA_cert
    })
  ]
}