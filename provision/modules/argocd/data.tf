data "keycloak_openid_client" "argocd-client-secret" {
  realm_id  = "k8s-lab"
  client_id = "argocd"
}