variable "iprange" {
  type = string
}

variable "metallb" {
  type = object({
    helm_release_name  = string
    helm_repo_url      = string
    helm_chart_name    = string
    helm_chart_version = string
    release_namespace  = string
  })
}