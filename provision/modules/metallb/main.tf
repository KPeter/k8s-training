resource "kubernetes_namespace" "metallb" {
  metadata {
    name = var.metallb.release_namespace
  }
}

resource "kubectl_manifest" "metallb-addresspool" {
  yaml_body  = templatefile("${path.module}/manifests/addresspool.yaml", { iprange = var.iprange, namespace = var.metallb.release_namespace })
  depends_on = [helm_release.metallb]
}

resource "kubectl_manifest" "metallb-advertisement" {
  yaml_body  = templatefile("${path.module}/manifests/advertisement.yaml", { namespace = var.metallb.release_namespace })
  depends_on = [helm_release.metallb]
}

resource "helm_release" "metallb" {
  name       = var.metallb.helm_release_name
  repository = var.metallb.helm_repo_url
  chart      = var.metallb.helm_chart_name
  version    = var.metallb.helm_chart_version
  namespace  = var.metallb.release_namespace
}
