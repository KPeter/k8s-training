
This lab is intended to introduce Crossplane to create external Cloud resources and manage their life-cycle inside Kubernetes. 

Topics:
* Install Crossplane and set up basics
* Create simple AWS resources using aws-provider
* Create simple AWS resources using Terraform (inline)
* Create composite resources with CRDs, Compositions and Claims

# Install Crossplane

To install Crossplane please follow the official documentation: [Install guide](https://docs.crossplane.io/latest/software/install/)
```
$ helm repo add crossplane-stable https://charts.crossplane.io/stable
$ helm repo update
$ helm install crossplane --namespace crossplane-system --create-namespace crossplane-stable/crossplane
```

## Set up lab providers

### Create AWS provider and providerConfig resources

* Create an aws user for Crossplane (or use your own)
* Create a secret and access key for the user
* Create a role for Crossplane to manage ec2 objects
* Assign the role to your user
* Create a kuberetes secret with the aws credential
  * Create a credential file like /tmp/awscred.ini
  ```
  [default]
  aws_access_key_id = <your access key here>
  aws_secret_access_key = <your secret key here>
  ```
  * Create the secret
  ```
  kubectl create secret generic aws-secret -n crossplane-system --from-file=creds=/tmp/awscred.ini
  ```
* Add the providers and the configuration
```
$ kubectl apply -f basicinfra/crossplane/AWS-simple/provider-and-config/aws-provider-ec2.yaml
$ kubectl apply -f basicinfra/crossplane/AWS-simple/provider-and-config/aws-providerconfig.yaml
```

### Create Terraform provider and config

It uses the same secret in the provider config that we have created earlier

```
$ kubectl apply -f basicinfra/crossplane/Terraform-simple/provider-and-config/tf-provider.yaml
$ kubectl apply -f basicinfra/crossplane/Terraform-simple/provider-and-config/tf-providerconfig.yaml
```

# Deploy AWS resources with AWS EC2 provider

* Deploy the VPC
```
$ kubectl apply -f basicinfra/crossplane/AWS-simple/resources/aws-single-vpc.yaml
```
* Deploy the subnets (we reference the VPC in the manifest)
```
$ kubectl apply -f basicinfra/crossplane/AWS-simple/resources/aws-single-subnets.yaml
```
* Deploy the instance (we reference the subnet in the manifest)
```
$ kubectl apply -f basicinfra/crossplane/AWS-simple/resources/aws-single-instances.yaml
```
* Create the EBS and the volume attachment (in the attachment we reference the instance and the ebs)
```
$ kubectl apply -f basicinfra/crossplane/AWS-simple/resources/aws-single-ebs.yaml
$ kubectl apply -f basicinfra/crossplane/AWS-simple/resources/aws-single-volumeattachment.yaml
```

* Check K8S for the created resources
  * Troubleshoot if something went wrong. Crossplane produces the API errors in the resource description (kubectl describe)
* Check your AWS account for the resources

## Pros and Cons

The above installation method has pros and cons.

**PROS**:
* Simple resource definitions

**CONS**:
* Many resources needs to be managed in kubernetes
* Must handle the AWS resource dependencies manually (first delete the attachment and the instance and the subnets and the instance and finally the vpc)
* Must specify the references manually
* Every resource must be created one-by-one manually (no templates, no packages)
* There is no "loop" capability to create many resources in one step

# Deploy AWS resources with Terraform provider

* There is an inline and a remote workspace definition
* The example uses an s3 bucket for storing the state but if you don't want to store it there you can just comment the section and enable the previous one that uses Kubernetes secret
  * So if you want to use S3 as a backend you must set up an S3 bucket first
* The remote example uses a private git repository that needs a credential
  * As a prerequisite a .git-credential file content needs to be added to a secret (eg.: https://<username>:<token>@gitlab.com)
  * Since in the example it is a private repo please create your own and update the source url
    * The content of the repo could be found under [this directory](../../basicinfra/crossplane/Terraform-simple/resources/remote_git_content)

```
$ kubectl apply -f basicinfra/crossplane/Terraform-simple/provider-and-config/tf-provider.yaml
# Before apply the provider config update the state section to point to your S3 bucket
$ kubectl apply -f basicinfra/crossplane/Terraform-simple/provider-and-config/tf-providerconfig.yaml
```

* Deploy resources

```
$ kubectl apply -f basicinfra/crossplane/Terraform-simple/resources/tf-workspace.yaml
```

* Check the state
```
$ aws s3api list-objects --bucket hrktfstates
{
    "Contents": [
        {
            "Key": "env:/sample-inline/crossplane-simple-test.tfstate",
            "LastModified": "2024-01-25T09:58:15+00:00",
            "ETag": "\"b0a252d3d5637547a35445a778ebb9e1\"",
            "Size": 3745,
            "StorageClass": "STANDARD",
            "Owner": {
                "DisplayName": "<redacted>",
                "ID": "5b7c6f22e9f51cac17813c8925dc3ba982263100375efd9ef2ef67e102dffc00"
            }
        }
    ],
    "RequestCharged": null
}
```
* Check the VPC
```
$ aws ec2 describe-vpcs
{
    "Vpcs": [
        {
            "CidrBlock": "10.0.0.0/16",
            ...
            "IsDefault": false,
            "Tags": [
                {
                    "Key": "Name",
                    "Value": "crossplane-test-tf"
                }
            ]
        },
        {
            "CidrBlock": "172.31.0.0/16",
    ...
  ]
}

```

## Pros and Cons

The above installation method has pros and cons.

**PROS**:
* Simple resource definitions
* Terraform can handle dependencies
* Terraform has "loop" capability to crate many resources in a single step

**CONS**:
* Terraform :D


# Deploy AWS resources with composite resources

With composite resources we can use compositions as templates to deploy infrastructure elements as parameterized packages.
More about composite resources: [Doc](https://docs.crossplane.io/latest/concepts/composite-resource-definitions/)


![How it works](images/composition-how-it-works.png "How it works")


We will see:
* How we can create composite resource definition as CRD
  *Composite resource definitions (XRDs) create/define a new schema for a custom API inside a Kubernetes cluster 
* How we can create composition templates exposing properties as parameters
  * A Composition configures how one or more custom resource should be rendered in response to the creation or modification of a custom resource defined
* How we can create Claims to populate the composite template with values to install infrastructure
  * Claims represents a set of managed resources as a single Kubernetes object, inside a namespace

## Create CRDs (composite resource definition)

```
$ kubectl apply -f basicinfra/crossplane/AWS-composition/crds/vpc/definition.yaml
```

Few important/interesting things to be mentioned:
* additionalPrinterColumns: specify hat additional columns should be displayed on "kubectl get"
* metadata.name: must be spec.names.plural+"."+spec.group in the manifest
* spec.group: the new API group that will be used when the claim will be created
* spec.group and spec.name: will be used in the composition for the compositeTypeRef 

## Create composition template

```
$ kubectl apply -f basicinfra/crossplane/AWS-composition/compositions/vpc/vpc-composition.yaml
```

Few important/interesting things to be mentioned:
* writeConnectionSecretsToNamespace: is used when the resource creation gives back some parameters like when we create a PaaS database and will have the connection string ans password
* compositeTypeRef: must much the type in the CRD
* patchSets: are good if you want to patch all the resources in the same way in the composition (eg.: all will have tha same provider configuration, or same costcenter tags)
* base.spec.forProvider: shouldn't be provided if all the provider fileds are generated (with Patches or patchSets)
* You can use very complex methods in Patches or patchSets to generate fields


## Create claim

```
$ kubectl apply -f basicinfra/crossplane/AWS-composition/claims/vpc-claim.yaml
```

* Check resources
```
$ kubectl get VPC
NAME                        READY   SYNCED   EXTERNAL-NAME           AGE
dtmb-test-vpc-8b25t-25k94   True    True     vpc-0ace2d832fa649441   45s
```
```
$ kubectl get VPC dtmb-test-vpc-8b25t-25k94 -o jsonpath='{.status.atProvider.arn}'
arn:aws:ec2:us-east-1:205363380413:vpc/vpc-0ace2d832fa649441
```
```
$ aws --profile=home ec2 describe-vpcs --vpc-ids vpc-0ace2d832fa649441 | jq '.Vpcs[].Tags[]'
{
  "Key": "crossplane-name",
  "Value": "dtmb-test-vpc-8b25t-25k94"
}
{
  "Key": "crossplane-providerconfig",
  "Value": "aws-provider-config"
}
{
  "Key": "Name",
  "Value": "dtmb-test-vpc-vpc"
}
{
  "Key": "crossplane-kind",
  "Value": "vpc.ec2.aws.upbound.io"
}
```

# Deploy resources with Terraform composite resources

We do the same as we did with the AWS provider but now we use a composition for Terraform provider.
Notes:
* variables are put into a ConfigMap (a Secret also could be used)
* variables file is in json format (just for fun)
* the CRD define the git-repository paameter and the parameters for referencing the variables file

Create the variables file:
```
$ echo '{ "vpc_cidr": "10.14.0.0/24", "vpc_name": "crspln-remote-composition" }' > /tmp/terraform.tfvars.json
$ kubectl create configmap -n crossplane-system tf-vars-composition-test --from-file=/tmp/terraform.tfvars.json
```

## Create the CRD

```
$ kubectl apply -f basicinfra/crossplane/Terraform-composition/crds/definition.yaml
```

## Create the Composition

```
$ kubectl apply -f basicinfra/crossplane/Terraform-composition/compositions/composition.yaml
```
NOTES:
* It creates a Terraform workspace as resource so you cannot check/see the VPC resource 


## Create the Claim

```
$ kubectl apply -f basicinfra/crossplane/Terraform-composition/claims/claim.yaml
```


