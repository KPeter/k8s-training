# LAB :: Building Simple Prometheus Stack

In this lab we will build a Promtheus stack using the operator. The "kube-prometheus-stack" offers many options but we will use only the opertaor install from it and any other resources will be created as separate resources with their kind.

## Install Prometheus operator

You can install it as an ArgoCd app
```
$ kubectl apply -f gitops-apps/prometheus-operator-app.yaml
```

Now, don't forget to sync the application on the UI or with argocd cli
```
$ argocd app sync argocd/prometheus-operator
```

## Start the mocserver

This will start a webserver that accepts GET and POST request :D 
The server requires python3 installed

```
$ custom-apps/prometheus-operator/mocserver/basic_webserver.py
```
You should see that the server has been started and listening.
Now get your host IP address with "ip" or "ifconfig" command. This will be your physical card address (wifi or ethernet interface)


## Install Prometheus server and AlertManager

NOTE: You must change the manifest for AlertmanagerConfig to have the real IP of your host machine where the MOC webhook server will run before apply!!! To do that you can work on your git repo fork.

We create prometheus server with the following manifests:
* Some additional rbac rules: [prometheus-rbac.yaml](../../custom-apps/prometheus-operator/resources/prometheus-rbac.yaml)
* Prometheus instance: [prometheus-server.yaml](../../custom-apps/prometheus-operator/resources/prometheus-server.yaml)
* The ingress: [prometheus-ingress.yaml](../../custom-apps/prometheus-operator/resources/prometheus-ingress.yaml)

For alertmanager we create:
* Alertmanager config: [alertmanager-config.yaml](../../custom-apps/prometheus-operator/resources/alertmanager-config.yaml)
* Alertmanager instance: [alertmanager-server.yaml](../../custom-apps/prometheus-operator/resources/alertmanager-server.yaml)
* Alertmanager ingress: [alertmanager-ingress.yaml](../../custom-apps/prometheus-operator/resources/alertmanager-ingress.yaml)


We install the above with ArgoCD or you can install them manually by kubectl with the proper order (see above). In ArgoCD we use syncwave to specify the order.

```
$ kubectl apply -f gitops-apps/prometheus-stack-app.yaml
```

Now, don't forget to sync the application on the UI or with argocd cli
```
$ argocd app sync argocd/prometheus-stack
```

## Test 

Run curl to send an alert to Alertmanager

```
$ curl -k -H 'Content-Type: application/json' -d '[{"labels":{"alertname":"grpc_alert"}}]' https://alertmanager.k8s.local/api/v1/alerts
{"status":"success"}
```

After a while you sould see the message on the console where the webserver was started
```
Server started http://0.0.0.0:5000
MY SERVER: I got a POST request.
POST request
Path: /
Body:{"receiver":"prometheus/poppyseeds/mocserver","status":"firing","alerts":[{"status":"firing","labels":{"alertname":"grpc_alert"},"annotations":{},"startsAt":"2023-10-25T16:53:19.768693459Z","endsAt":"0001-01-01T00:00:00Z","generatorURL":"","fingerprint":"69b06d4eb7468a87"}],"groupLabels":{},"commonLabels":{"alertname":"grpc_alert"},"commonAnnotations":{},"externalURL":"http://alertmanager-alertmanager-0:9093","version":"4","groupKey":"{}/{alertname=\"grpc_alert\"}:{}","truncatedAlerts":0}
```

