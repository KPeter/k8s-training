# Create keycloak required objects using the CLI

The following is an example how you can use the admin API. You don't need to use it if you enabled the config-cli in values.yaml or you installed keycloak using Terraform.

```
# Get admin user pwd
$ ADMINUSER="<get it from the Helm values.yaml file>"
$ ADMINPW="<get it from the secret>"
# Get access token from keycloak
$ TOKEN=$(curl -s -k -H "Content-Type: application/x-www-form-urlencoded" https://kc-console.nginx.k8s.local/realms/master/protocol/openid-connect/token -d "grant_type=password&client_id=admin-cli&username=$ADMINUSER&password=$ADMINPW" | jq -r '.access_token')
# Set up an alies for curl and auth
$ alias kurl='curl -k -s -H "Content-Type: application/json" -H "Authorization: Bearer ${TOKEN}"' 
```
NOTE: token expires after a period (1 min), so if you get "401 Unauthorized" response just run TOKEN acquire command again

## Create the realm

```
$ kurl -X POST https://keycloak.k8s.local/admin/realms --data-binary @- << EOF                       
{
"id": "k8s-lab",
"realm": "k8s-lab",
"enabled": true
}
EOF
```

## Create the group scope

```
$ kurl -X POST https://keycloak.k8s.local/admin/realms/k8s-lab/client-scopes --data-binary @- << EOF
{                                                                                            
"id": "k8s-groups",
"name": "groups",  
"description": "Scope for groups",
"protocol": "openid-connect",
"attributes": {
  "include.in.token.scope": "true",
  "display.on.consent.screen": "true"
  }
}  
EOF
```

## Set the client scope to default

```
$ kurl -X PUT https://keycloak.k8s.local/admin/realms/k8s-lab/default-default-client-scopes/k8s-groups
```

## Create the mapper for groups scope

```
$ kurl -X POST https://keycloak.k8s.local/admin/realms/k8s-lab/client-scopes/k8s-groups/protocol-mappers/add-models --data-binary @- << EOF
[{
  "protocol": "openid-connect",
  "protocolMapper": "oidc-group-membership-mapper",
  "name": "groups",
  "config": {
    "claim.name": "groups",
    "full.path": "false",
    "id.token.claim": "true",
    "access.token.claim": "true",
    "userinfo.token.claim": "true"
  }
}]
EOF
```

We need to create new client with the proper mappings. You can use the following JSON files with the API to crete all the resources

* The client scope and mappers
```
[
  {
    "id": "kubernetes-client-scopes",
    "name": "kubernetes-client-scopes",
    "description": "Kubernetes client scopes",
    "protocol": "openid-connect",
    "attributes": {
      "include.in.token.scope": "true",
      "display.on.consent.screen": "true"
    },
    "protocolMappers": [
      {
        "name": "kubernetes-audience",
        "protocol": "openid-connect",
        "protocolMapper": "oidc-audience-mapper",
        "config": {
          "included.client.audience": "kubernetes",
          "id.token.claim": "true",
          "access.token.claim": "true"
        }
      },
      {
        "name": "kubernetes-groups",
        "protocol": "openid-connect",
        "protocolMapper": "oidc-group-membership-mapper",
        "config": {
          "full.path": "false",
          "userinfo.token.claim": "true",
          "multivalued": "true",
          "id.token.claim": "true",
          "access.token.claim": "true",
          "claim.name": "groups"
        }
      }
    ]
  }
]
```