# Create keycloak required objects using the UI

Use the admin username and password that you defined in the values file (default: administrator/Adm1nPassw0rd)

* k8s-lab realm
  ![create realm](../images/keycloak/create_realm.png)
* Create 'groups' client scope as a default scope
  * Create new scope
  ![create client scope 1](../images/keycloak/create_client_scope_1.png)
  * Name it as 'groups' and set it to 'Default' to assign it to any new clients automatically
  ![create client scope 2](../images/keycloak/create_client_scope_2.png)
  * After the scope has been created create the mapper for it
  ![create client scope 3](../images/keycloak/create_client_scope_3.png)
  * Choose 'Group membership'
  ![create client scope 4](../images/keycloak/create_client_scope_4.png)
  * Set up the mapper
  ![create client scope 5](../images/keycloak/create_client_scope_5.png)
* Create ArgoCD client for OIDC
  * Create ArgoCD client
  ![create client 1](../images/keycloak/create_client_1.png)
  ![create client 2](../images/keycloak/create_client_2.png)
  ![create client 3](../images/keycloak/create_client_3.png)
  * Note the client secret on the Client->Credentials tab in 'Client secret' blade. You must use it in ArgoCD helm manifest
  ![create client 4](../images/keycloak/create_client_4.png)
* Create an 'admin'group
  ![create group 1](../images/keycloak/create_group_1.png)
  ![create group 2](../images/keycloak/create_group_2.png)
* Create a user in the realm as a member of the admin group
  * Don't set up required action
  * set the email verified
  * Join the user to the admin group
  ![create user](../images/keycloak/create_user.png)
  * After it has been created set up the password under Credentials tab
  ![create password](../images/keycloak/create_password.png)