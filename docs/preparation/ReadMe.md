# Installing and starting Minikube

Install minikube with using the documentation (I used it on Linux so the backend is KVM).

* Set up minikube resources (we need at least 8GB RAM and 4-6 vCPU if we use extra storage provider)

```
$ minikube start -p lab --memory 8g --cpus 4 --disk-size 40g
```

# Install tools on your host machine

* Install kubectl with using the doc: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
* Install Helm with using the doc: https://helm.sh/docs/intro/install/

NOTE: If you want to use Terraform to provision services use the doc: https://developer.hashicorp.com/terraform/downloads

## Notes on Terraform

There is a chicken-egg problem with Terraform kubernetes provider. If you try to use kubernetes_manifest resource it checks if the resource could be created through CRDs. But if your installation needs to create those CRDs of course they couldn't be found in plan or apply plan. 
The error message is like: no matches for kind "xxxx" in group "yyyy" 
 
To solve this problem we use kubectl provider that doesn't check the CRDs just try to create the resource.

Also we cannot put a module into the provision directory since Terraform somehow wants to use the helm modules from that directory if the module name and the chart name is the same (very strange bug). So we moved all the modules under the modules directory.

# Prepare the Virtual network and minikube

Set up a smaller DHCP range to have fix IP range for metalLB:
```
# stop minikube 
$ minikube stop -p lab
# get the vnets
$ virsh net-list
# Get the definition of minikube vnet
$ virsh net-dumpxml mk-lab | grep range
# Change the dhcp range to have a fix ip range for metallb eg.:
$ virsh net-update mk-lab delete ip-dhcp-range "<range start='192.168.39.2' end='192.168.39.254'/>" --live --config
$ virsh net-update mk-lab add ip-dhcp-range "<range start='192.168.39.150' end='192.168.39.254'/>" --live --config
# start minikube 
$ minikube start -p lab
```

One more thing. Let's enable metrics-server addon

```
$ minikube addons enable metrics-server -p lab
```

# Install MetalLb

This will install MetalLb. Please change "metallb-values" correspond to your minikube VM network range. Use the range from the previous step (eg.: 192.168.39.2-192.168.39.149)

```
$ helm repo add metallb https://metallb.github.io/metallb
$ helm install --create-namespace -n metallb metallb metallb/metallb
$ kubectl apply -f basicinfra/metallb/metallb-iprange.yml
```

OR with Terraform
```
$ cd provision
$ terraform apply -target=module.metallb
``` 
NOTE: If the IP range is different please update "iprange" in the main.tf in the metallb module section


# Install Traefik

```
$ helm repo add traefik https://helm.traefik.io/traefik
$ helm install --create-namespace -n traefik traefik traefik/traefik -f basicinfra/traefik/traefik-values.yml
```

OR with Terraform
```
$ cd provision
$ terraform apply -target=module.traefik
``` 

# Install Ingress-nginx

In kubernetes you can use more ingress controllers simultaneously. Traefik is very good at providing basic ingress functionalities, but Nginx has more features. So if you prefer Nginx instead Traefik you can use it instead, however we use both in the examples. 
We will use the community maintained nginx ingress controller and not the Nginx Inc. provided controller in the Lab

```
$ helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
$ helm install --create-namespace -n ingress-nginx ingress-nginx ingress-nginx/ingress-nginx -f basicinfra/ingress-nginx/values.yaml
```

OR with Terraform
```
$ cd provision
$ terraform apply -target=module.ingress-nginx
```

# Update your Linux Dnsmasq setting to point to Traefik load-balanced IP and externalDNS provider

In this setup we have LB IPs that are used by Traefik and Nginx and all the service could be reached with ingresses under taefik.k8s.local or nginx.k8s.local fqdn. In addition, we will setup externalDNS later to manage a different domain k8s.lab through a DNS server on the host (CoreDNS or PowerDNS).
Also we will add your host primary IP since if a service is tarted on your host we need to resolve it's address by a name inside kubernetes.In this case if you specify "hostname.local" in a config inside or outside kubernetes it will be resolved to your primary IP always (eg.: for externalDNS providers)

``` 
# get the Traefik service LB ip
$ kubectl get svc -n traefik -o jsonpath='{.items[0].status.loadBalancer.ingress[0].ip}'
# get the Nginx service LB ip
$ kubectl get svc -n ingress-nginx -o jsonpath='{.items[0].status.loadBalancer.ingress[0].ip}'

# Add line to /etc/NetworkManager/dnsmasq.d/k8s.conf with the proper IP into the address section
# > address=/traefik.k8s.local/192.168.39.2
# > address=/nginx.k8s.local/192.168.39.3
# > address=/hostmaster.local/<YOU HOST PRIMARY IP>
# > server=/k8s.lab/127.0.0.1#1053
# NOTE: make sure you added your host primary IP as hostmaster.local
# The above: 
# sends the requests to traefik LB IP if the domain is traefik.k8s.local AND
# sends the requests to nginx LB IP if the domain is nginx.k8s.local AND
# sends the request to PowerDNS as externalDNS provider if the domain is k8s.lab (see later)

# restart NetworkManager service
$ sudo systemctl restart NetworkManager

# add NetworkManager as local resolver to systemd-resolved (/etc/systemd/resolved.conf)
# add the below lines to [Resolve] section
# > DNS=127.0.1.1
# > Domains=~lab ~local
# The above add dnsmasq to resolve any addresses for the domains k8s.local and k8s.lab

# Restart systed-resolved
$ sudo systemctl restart systemd-resolved

# Test
$ dig something.traefik.k8s.local
...
; ANSWER SECTION:
something.traefik.k8s.local.    0       IN      A       192.168.39.2
....

$ dig something.nginx.k8s.local
...
; ANSWER SECTION:
something.nginx.k8s.local.    0       IN      A       192.168.39.3
....

```

To test the k8s.lab domain we must install CoreDNS or PowerDNS first. Eg.: PowerDNS refuse any queries before we add the zone (see later)

# Install externalDNS with the preferred provider

Since Terraform doesn't have podman provider and the available docker providers have only very basic functionalities we must do all the steps manually.
There is a plan to use ansible for automate the tasks. 
Any Terraform or Ansible related contribution is welcome. :D

You can use CoreDNS or PowerDNS as local DNS server but due to the complexity of the CoreDNS installation I suggest to use PowerDNS instead.

## CoreDNS provider

Install etcdctl on host
```
# On debian/ubuntu based distribution
$ sudo apt install etcd-client
```

Get minikube's etcd key and certificate
```
# Allow minikube docker user to read the TLS files
$ ssh -i $(minikube ssh-key -p lab) docker@$(minikube ip -p lab) "sudo setfacl -R -m u:docker:rX /var/lib/minikube/certs/etcd"
# Copy the files out from minikube
$ mkdir externalDNS/etcd
$ scp -i $(minikube ssh-key -p lab) docker@$(minikube ip -p lab):/var/lib/minikube/certs/etcd/server.key basicinfra/externalDNS/coreDNS_provider/etcd/etcd.key
$ scp -i $(minikube ssh-key -p lab) docker@$(minikube ip -p lab):/var/lib/minikube/certs/etcd/server.crt basicinfra/externalDNS/coreDNS_provider/etcd/etcd.crt
$ scp -i $(minikube ssh-key -p lab) docker@$(minikube ip -p lab):/var/lib/minikube/certs/etcd/ca.crt basicinfra/externalDNS/coreDNS_provider/etcd/etcd-ca.crt
```

Test that you can read minikube etcd
```
$ ETCDCTL_API=3 etcdctl --endpoints "https://$(minikube ip -p lab):2379" --cacert=basicinfra/externalDNS/coreDNS_provider/etcd/etcd-ca.crt --key=basicinfra/externalDNS/codeDNS_provider/etcd/etcd.key --cert=basicinfra/externalDNS/coreDNS_provider/etcd/etcd.crt  get  / --prefix --keys-only
```
You should see all the keys

Now you can start a coreDNS docker container
```
$ basicinfra/externalDNS/coreDNS_provider/coreDNS.sh start
``` 
NOTE: you can delete coreDNS if you don't need it anymore with basicinfra/externalDNS/coreDNS_provider/coreDNS.sh cleanup

## Install PowerDNS

It is much simpler as you can see :D

Start the container
```
$ basicinfra/externalDNS/powerDNS_provider/powerDNS.sh start
```
NOTE: you can delete powerDNS if you don't need it anymore with basicinfra/externalDNS/powerDNS_provider/powerDNS.sh cleanup

This above will generate the PDNS api key if you want (uncomment line 9, and comment line 11) but then you need to change the externalDNS manifest accordingly, to do that get the key
```
$ PDNS_API_KEY=$(podman inspect powerdns | jq -r '.[].Config.Env[]' | awk -F= '{if(/PDNS_AUTH_API_KEY/) {print $2}}')
```

Add the k8s.lab zone to PowerDNS
```
$ curl -X POST -H "X-API-Key: ${PDNS_API_KEY}" "http://127.0.0.1:8081/api/v1/servers/localhost/zones" -d '{"name":"k8s.lab.", "kind":"Native", "masters": [], "nameservers": ["ns1.k8s.lab.", "ns2.k8s.lab."]}'
```

Now update the basicinfra/externalDNS/powerDNS_provider/extdns-powerdns.yaml deployment manifest with the above api key and your host IP (line 25-26). 


## Install externalDNS 

First create the namespace since we need to create th etcd secret before the installation
```
$ kubectl create ns externaldns
```

### With coreDNS
Then create the etcd certificates secret must be referenced in the values file
```
$ kubectl -n externaldns create secret generic etcd-cert-secret --from-file=basicinfra/externalDNS/coreDNS_provider/etcd
```

Now install externalDNS with coreDNS
```
$ helm install --create-namespace -n externaldns externaldns bitnami/external-dns -f basicinfra/externalDNS/coreDNS_provider/extdns-coredns-values.yaml
```

### With powerDNS

#### Manual steps

First create the RBAC rules
```
$ kubectl apply -f basicinfra/externalDNS/powerDNS_provider/rbac-extdns.yaml
```

Now Install externalDNS with powerDNS (just don't forget to update the manifest with the randomized api key if you chose that path)
```
$ kubectl apply -f basicinfra/externalDNS/powerDNS_provider/extdns-powerdns.yaml
```

#### Terraform 

Edit the main.tf to change the extdns module apikey parameter then 
```
$ cd provision
$ terraform apply -target=module.externaldns
```


# Install cert-manager

During this step I will create my own CA certificate to be used for generating TLS certificates for services. After I installed cert-manager I will add the CA cert as cluster issuer.
You can skip the CA cert creation step (starting with "opennssl req...") if you want to use the already generated one (could be found in the repo).

```
$ helm repo add jetstack https://charts.jetstack.io
$ helm install --create-namespace -n cert-manager cert-manager jetstack/cert-manager -f cert-manager/certmgr-values.yaml
## Create CA key and cert
$ openssl req -x509 -newkey rsa:4096 -sha256 -days 365 -nodes \
 -keyout basicinfra/cert-manager/ca.k8s.local.key -out basicinfra/cert-manager/ca.k8s.local.cer \
 -subj /O=MyOrg/OU=IT/CN=ca.k8s.local \
 -extensions ext \
 -config basicinfra/cert-manager/ca.conf
## Create CA secret
$ kubectl -n cert-manager create secret tls internal-ca --key basicinfra/cert-manager/ca.k8s.local.key --cert basicinfra/cert-manager/ca.k8s.local.cer
## Create cluster issuer
$ kubectl apply -f basicinfra/cert-manager/cluster-issuer.yaml
```

OR with Terraform
```
$ cd provision
$ terraform apply -target=module.cert-manager
``` 
NOTES: 
* Please set the variable "createnewcerts" to "true" in main.tf for the cert-manager module if you run the template at the first time
* Then change back into "false" (of course you can create the certs on every run later but I recommend to create the cert only once since later we will use it for several services and we don't need to reconfigure them)
* Terraform writes out the key and the cert into "certs" directory and the content also could be queried, eg.: "terraform output cert-manager-CA-cert"


# Install Keycloak

Install Keycloak with your preferred method using [this document](../03_k8s_oidc/ReadMe.md#install-keycloak)

# Install ArgoCD

You must update the ArgoCD values with ( [argocd-values.yaml](../../basicinfra/argocd/argocd-values.yaml) )
* The client secret that is generated to your client
* The CA certificate pem content if you created a new CA cert for cert manager 
  * Since the traffic must be secured between ArgoCD and Keycloak ArgoCD must trust Keycloak certificate. To make it right we need to add the CA cert as trusted to ArgoCD

Additionally
* In the values for the rbac policy we map the "admins" group as ArgoCD administrators

```
$ helm repo add argo https://argoproj.github.io/argo-helm
$ helm install --create-namespace -n argocd argocd -f argocd/argocd-values.yaml argo/argo-cd
```

OR with Terraform
```
$ cd provision
$ terraform apply -target=module.argocd
``` 

NOTE on SSO: Since ArgoCD hasn't supported multiple "url" in the settings yet (https://github.com/argoproj/argo-cd/issues/5388) you should use https://argocd.traefik.k8s.local in configuration (since this is the parameter in the values for 'url') because SSO works only with that URL.

## Setup ArgoCD

### ArgoCD CLI usage 

If you want to use argcd cli you must install it first then set it up. 
* For the installation just follow the documantation page
* To log in:
  * Get the builtin admin user password first
  ```
  $ kubectl get secret -n argocd argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d; echo
  ```
  * Login:
  ```
  $ argocd --insecure --username=admin login argocd.traefik.k8s.local
  # and provide the password
  ```

Happy argocding :D

### Configuration 

After you set up everything you should be able to log in with the created adminuser with the password with clicking on the "Log in via Keycloak" button using URL: https://argocd.k8s.local

* First add the repo to ArgoCD globaly
  ```
  $ argocd repo add https://gitlab.com/talentiaacademy/k8s-training.git
  ``` 

* Create a project
  ```
  $ argocd proj create k8s-lab --description "All k8s-lab applications" -d https://kubernetes.default.svc,\*
  ```
  OR
  ![create project](../images/argocd/create_new_project.png)
* In "Source Repositories" section click EDIT to add new repo
  ```
  $ argocd proj add-source k8s-lab https://gitlab.com/talentiaacademy/k8s-training.git
  ```
  OR
  ![add source repo](../images/argocd/add_source_repo.png)
* In "Destination" section allow all namespaces to be used for deploying
  ![specify destination](../images/argocd/specify_destination.png)
* In "Cluster resource allow list" section allow to create all kubernetes kinds on all API group (without this deployments will fail. This very open "allow" is good for our lab but in production you can consider more secure settings)
  ```
  $ argocd proj allow-cluster-resource k8s-lab "*" "*"
  ```
  OR
  ![create allow list](../images/argocd/create_allow_list.png)


> # ✋ Now you can continue with Labs (see under docs)