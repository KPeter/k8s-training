#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

echo $SCRIPT_DIR

start() {
  podman run -d --user 0 --name coredns -v ${SCRIPT_DIR}/config:/config -v ${SCRIPT_DIR}/etcd:/etcd -e ETCD_IP=$(minikube ip -p lab) -e ETCD_PORT=2379 -p 127.0.0.1:1053:53/udp coredns/coredns -conf /config/Corefile
}

stop() {
  podman stop coredns
}

cleanup() {
  stop
  podman rm coredns
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  cleanup)
    cleanup
    ;;
  *)
    echo "Usage: $0 {start|stop|cleanup}"
esac
