#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

echo $SCRIPT_DIR

start() {
  ## For secure API key
  #APIKEY=$(base64 < /dev/urandom | head -c 16)
  ## But we use the easiest way :D
  APIKEY=verysecure1234
  podman run --user 0 -d -p 127.0.0.1:1053:53 -p 127.0.0.1:1053:53/udp -p 8081:8081 --name=powerdns -e PDNS_AUTH_API_KEY=$APIKEY  powerdns/pdns-auth-master
}

stop() {
  podman stop powerdns
}

cleanup() {
  stop
  podman rm powerdns
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  cleanup)
    cleanup
    ;;
  *)
    echo "Usage: $0 {start|stop|cleanup}"
esac
